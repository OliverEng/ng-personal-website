/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      fontFamily: {
        "Open-Sans": ["Open Sans", "sans-serif"],
      },
    },
  },
  plugins: [],
};
