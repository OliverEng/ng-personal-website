import { Component, OnInit } from '@angular/core';
declare var $: any;
let $section = $('.section');
let $window = $(window);
let fadeAt = 80; // start fade ad Npx from top
let zeroAt = 20; // Npx from top = 0 opacity

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css'],
})
export class HomePage implements OnInit {
  constructor() {}

  ngOnInit(): void {
    $section = $('.fade-top');
    $window = $(window);
  }
}

function fadeByApproach() {
  let st = $window.scrollTop();
  $.each($section, function (idx: number, el: any) {
    let $ch = $('*', el); // all elements
    $.each($ch, function (idx: number, sub_el: any) {
      let top = sub_el.offsetTop - st + el.offsetTop;
      let opa = 1;
      sub_el.style.opacity = opa.toString();
    });
    let secPos = el.offsetTop - st;
    if (secPos < fadeAt) {
      $.each($ch, function (idx: number, sub_el: any) {
        let top = sub_el.offsetTop - st + el.offsetTop;
        let opa = 1;
        sub_el.style.opacity = opa.toString();
        if (top < fadeAt) {
          opa = (top - zeroAt) / (fadeAt - zeroAt) / 1;
          sub_el.style.opacity = opa.toString();
        }
      });
    }
  });
}

$(window).on('scroll load resize', fadeByApproach);
