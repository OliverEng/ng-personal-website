import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  mobileMenuOpen: boolean = true;

  constructor() {}

  ngOnInit(): void {}

  //toggle menu
  toggleMobileMenu() {
    const mobileMenuButton = document.getElementById('mobile-menu-button')!;
    this.mobileMenuOpen = !this.mobileMenuOpen;
  }
}
